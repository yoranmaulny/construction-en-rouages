
gearWidth=30;
gearRadius=gearWidth/2;
gearPerimeter=2*gearRadius*PI;

woodThickness=3;

toothUpDown=0;
toothCount=10;
toothDepth=gearWidth/6;
//toothTopWidth=(gearPerimeter/2)/toothCount;
toothTopWidth=woodThickness*1.25;

module trapeze(l1,l2,h,larg) {
    polygon(points=[[l1/2,0],[-l1/2,0],[-l2/2,larg],[l2/2,larg]]);
}

module generateGear()
{
    difference()
    {
        circle(r=gearRadius,$fn=128);
        
        for(i = [0 : 1 : toothCount])
        {
            rotate([0,0,i*(360/toothCount)])
            translate([0,gearRadius-toothDepth])
            
           if (toothUpDown == 0)  
           {           
                trapeze(woodThickness,toothTopWidth,woodThickness*3,toothDepth);
            }
            else
            {    
                trapeze(toothTopWidth, woodThickness,woodThickness*3,toothDepth);
            }    
        }
    }
}

generateGear();