# Construction en rouages

Réalisation de rouage en bois, afin de pouvoir faire des constructions.

Ces rouages seront mis à disposition sur le stand de Vendôme atelier Numérique lors du festival Hacker Faire 2019.

C'est un moyen ludique de montrer les possibilités d'une découpeuse laser et sont utilisation dans l'industrialisation d'un objet.

La génération du modèle est produite par le script gearToy.scad. Les paramètres utilisables sont le diamètre du rouage et l'épaisseur du matériau.

![Le logo de Vendôme Atelier Numérique](image/gearToy-van.jpg "Le logo de Vendôme Atelier Numérique")

![Un crabe](image/gearToy-crabe.jpg "Un crabe")

![Un scorpion](image/gearToy-scorpion.jpg "Un scorpion")