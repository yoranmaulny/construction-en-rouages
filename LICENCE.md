![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/3.0/fr/88x31.png "Licence Creative Commons")

Ce(tte) œuvre est mise à disposition selon les termes de la [Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 France](http://creativecommons.org/licenses/by-sa/3.0/fr).